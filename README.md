# Kaniko Docker Builds

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/containers/kaniko-docker-build)


## Overview Information

This Guided Exploration was improved and converted to the [Kaniko GitLab CI Component here](https://gitlab.com/explore/catalog/guided-explorations/ci-components/kaniko). 

* **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

* **Publish Date**: 2020-04-06

* **Last Update**: 2024-05-25 (Convert to Component)

* **Exploration Version**: v2.1.0

* **GitLab Version Released On**: 12.10

## References and Featured In:

- Video Walkthrough: [Least Privilege Container Builds with Kaniko on GitLab](https://www.youtube.com/watch?v=d96ybcELpFs)
- The labelling and tagging methodology and code in this example is completely reusable with docker (including the method for extracting the latest git tag using a pre stage) and it can be observed with docker building a customer kaniko container here: https://gitlab.com/guided-explorations/containers/build-your-own-kaniko
- A practical example of using this method is here: https://gitlab.com/guided-explorations/containers/aws-cli-tools  
    
